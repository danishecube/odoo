[![Build Status](https://runbot.mawriderp.com/runbot/badge/flat/1/master.svg)](https://runbot.mawriderp.com/runbot)
[![Tech Doc](https://img.shields.io/badge/master-docs-875A7B.svg?style=flat&colorA=8F8F8F)](https://mawriderp.com/documentation/master)
[![Help](https://img.shields.io/badge/master-help-875A7B.svg?style=flat&colorA=8F8F8F)](https://mawriderp.com/forum/help-1)
[![Nightly Builds](https://img.shields.io/badge/master-nightly-875A7B.svg?style=flat&colorA=8F8F8F)](https://nightly.mawriderp.com/)

Mawrid
----

Mawrid is a suite of web based open source business apps.

The main Mawrid Apps include an <a href="https://mawriderp.com/page/crm">Open Source CRM</a>,
<a href="https://mawriderp.com/page/website-builder">Website Builder</a>,
<a href="https://mawriderp.com/page/e-commerce">eCommerce</a>,
<a href="https://mawriderp.com/page/warehouse">Warehouse Management</a>,
<a href="https://mawriderp.com/page/project-management">Project Management</a>,
<a href="https://mawriderp.com/page/accounting">Billing &amp; Accounting</a>,
<a href="https://mawriderp.com/page/point-of-sale">Point of Sale</a>,
<a href="https://mawriderp.com/page/employees">Human Resources</a>,
<a href="https://mawriderp.com/page/lead-automation">Marketing</a>,
<a href="https://mawriderp.com/page/manufacturing">Manufacturing</a>,
<a href="https://mawriderp.com/#apps">...</a>

Mawrid Apps can be used as stand-alone applications, but they also integrate seamlessly so you get
a full-featured <a href="https://mawriderp.com">Open Source ERP</a> when you install several Apps.


Getting started with Mawrid
-------------------------

For a standard installation please follow the <a href="https://mawriderp.com/documentation/14.0/administration/install.html">Setup instructions</a>
from the documentation.

To learn the software, we recommend the <a href="https://mawriderp.com/slides">Mawrid eLearning</a>, or <a href="https://mawriderp.com/page/scale-up-business-game">Scale-up</a>, the <a href="https://mawriderp.com/page/scale-up-business-game">business game</a>. Developers can start with <a href="https://mawriderp.com/documentation/14.0/developer/howtos.html">the developer tutorials</a>
