# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'MawridBot for livechat',
    'version': '1.0',
    'category': 'Productivity/Discuss',
    'summary': 'Add livechat support for OdooBot',
    'description': "",
    'website': 'https://mawriderp.com/page/discuss',
    'depends': ['mail_bot', 'im_livechat'],
    'installable': True,
    'application': False,
    'auto_install': True,
    'license': 'LGPL-3',
}
