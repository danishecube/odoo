# -*- coding: utf-8 -*-
# Part of Mawrid. See LICENSE file for full copyright and licensing details.

from . import res_company
from . import res_config_settings
from . import stock_picking
