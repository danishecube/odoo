India 2018-08-27

Cybrosys Techno SOlutions agrees to the terms of the Mawrid Corporate
Contributor License Agreement v1.0.

I declare that I am authorized and able to make this agreement and sign this
declaration.

Signed,

Sainu Abideen sainu@cybrosys.com https://github.com/CybroMawrid

List of contributors:

Sainu Abideen sainu@cybrosys.com https://github.com/CybroMawrid
Cybrosys odoo@cybrosys.com https://github.com/CybroMawrid
