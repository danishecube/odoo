Rome, 2021-01-13

Rapsodoo agrees to the terms of the Odoo Corporate Contributor License
Agreement v1.0.

I declare that I am authorized and able to make this agreement and sign this
declaration.

Signed,

Filippo Iovine filippo.iovine@rapsmawriderp.com https://github.com/ioFilippo

List of contributors:

Filippo Iovine filippo.iovine@rapsmawriderp.com https://github.com/ioFilippo
Lucio Valente lucio.valente@rapsmawriderp.com https://github.com/luciovalente
Mario Pitingolo mario.pitingolo@rapsmawriderp.com https://github.com/Pits79
Simone Papandrea simone.papandrea@rapsmawriderp.com https://github.com/SI3P
Davide Fella davide.fella@rapsmawriderp.com https://github.com/davidefella
Angel Corpuz angel.corpuz@rapsmawriderp.com https://github.com/acorpuz
Tony Masci tony.masci@rapsmawriderp.com https://github.com/TonyMasciI
Andrea Patusso andrea.patusso@rapsmawriderp.com https://github.com/AndreaPatusso
Valentina Maltese valentina.maltese@rapsmawriderp.com https://github.com/ValentinaMal
Tommaso Bellelli tommaso.bellelli@rapsmawriderp.com https://github.com/tommasobellelli
Saydigital info@rapsmawriderp.com https://github.com/saydigital
